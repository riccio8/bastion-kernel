﻿namespace Bastion.Kernel
{
    using System;

    public abstract class Disposable : IDisposable
    {

        private const string DisposedErrorMessage = "Cannot access a disposed object.";

        private bool isDisposed;
        protected virtual bool IsDisposed => this.isDisposed;
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            this.isDisposed = true;
        }

        protected void DisposedThrow()
        {
            this.DisposedThrow(this.GetType().FullName);
        }
        protected void DisposedThrow(string className)
        {
            if (this.isDisposed)
            {
                throw new ObjectDisposedException(className, DisposedErrorMessage);
            }
        }
    }
}