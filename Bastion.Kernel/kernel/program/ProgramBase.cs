﻿namespace Bastion.Kernel
{
    using System.Text.RegularExpressions;
    using NLog;
    using System;
    using System.IO;
    using System.Threading;
    using Microsoft.Extensions.Configuration;
    using SimpleInjector;

    public abstract class ProgramBase : Disposable
    {
        private static readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        protected ProgramBase()
        {
            CfgKernel.ConfigureApplication();

            this.Configuration = CfgKernel.Configuration;
        }

        private string progName;
        public virtual string Name
        {
            get
            {
                if (progName == null)
                {
                    var typeName = this.GetType().Name;
                    Match match = Regex.Match(typeName, "^(.+)Program$");
                    if (match.Success) progName = match.Groups[1].Value;
                    else
                        throw new Exception(
                            "Please specify program name, either overriding Name property or name program by template <ProgramName>Program");
                }

                return progName;
            }
        }

        /// <summary>
        /// Gets root container for all class instances.
        /// </summary>
        public Container Root { get; } = new Container();

        protected IConfiguration Configuration { get; }

        protected TextWriter MessageStream { get; set; }

        protected int? ExitCode { get; set; }

        protected static CancellationToken CancelToken => cancellationTokenSource.Token;

        public int Run(string[] args)
        {
            try
            {
                AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
                this.Initialize();
                this.Execute(args);

                if (this is ConsoleProgram && !this.ExitCode.HasValue)
                {
                    WaitForTermination();
                }

                this.OnFinish();
                return this.ExitCode ?? 0;
            }
            catch (Exception ex)
            {
                try
                {
                    logger.Error(ex, "Exception");
                    this.OnError(ex);
                }
                catch
                {
                    
                }
                return 1;
            }
        }

        private void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine("OnUnhandledException " + e.ExceptionObject);
            logger.Error("OnUnhandledException " + e.ExceptionObject);
            CfgKernel.ShutdownLogger();
            Environment.Exit(1);
        }

        public void Terminate()
        {
            this.OnTerminating();

            cancellationTokenSource.Cancel();
        }

        public static bool WaitForTermination(int timeoutMs = Timeout.Infinite)
        {
            return CancelToken.WaitHandle.WaitOne(timeoutMs);
        }

        protected static int MainCommon<T>(string[] args) where T : ProgramBase, new()
        {
            try
            {
                using (T program = new T())
                {
                    return program.Run(args);
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Application initialization failed. " + ex);
                return 1;
            }
        }

        protected abstract void Execute(string[] args);

        protected virtual void PrintWelcomeMessage()
        {
        }

        protected virtual void Initialize()
        {
        }

        protected virtual void OnTerminating()
        {
            logger?.Info("Terminated.");
        }

        protected virtual void OnFinish()
        {
            if (!CfgKernel.NoAuxOutput)
            {
                this.MessageStream?.WriteLine($"{this.Name} exited.");
            }
            logger?.Info("Finished.");
        }

        protected virtual void OnError(Exception ex)
        {
            logger?.Error(ex, "Main application error.");
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                    cancellationTokenSource?.Dispose();

                    CfgKernel.ShutdownLogger();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }
    }
}