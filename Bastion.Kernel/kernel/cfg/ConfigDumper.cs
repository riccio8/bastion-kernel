﻿namespace Bastion.Kernel
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Text.RegularExpressions;
    using Microsoft.Extensions.Configuration;

    public class ConfigDumper
    {
        private readonly Action<string> dumpAction;

        /// <summary>
        /// if true, replace all values with path containing PASSWORD (case insensitive) with PasswordMask
        /// and hide password in connection string
        /// </summary>
        public bool HidePasswords { get; set; } = true;

        /// <summary>
        /// password replacement if HidePasswords is true
        /// </summary>
        public string PasswordMask { get; set; } = "<PASSWORD HIDDEN>";

        public ConfigDumper(Action<string> dumpAction)
        {
            this.dumpAction = dumpAction;
        }

        private void DumpConfig(IConfiguration config,
            IList<IConfigurationProvider> providers)
        {
            foreach (IConfigurationSection section in config.GetChildren())
            {
                StringBuilder sb = new StringBuilder(section.Path);
                string val = section.Value;

                if (val != null)
                {
                    if (HidePasswords)
                    {
                        val = section.Key.ToLowerInvariant().Contains("password")
                            ? PasswordMask
                            : Regex.Replace(val, "( Password=).+?; ", $"$1{PasswordMask}; ");
                    }

                    sb.Append('=').Append(val);
                }

                IConfigurationProvider provider = FindLastProvider(section.Path, providers);
                if (provider is NetEscapades.Configuration.Yaml.YamlConfigurationProvider yamlProv)
                {
                    sb.Append(" (from ").Append(yamlProv.Source.Path).Append(')');
                }

                dumpAction(sb.ToString());
                DumpConfig(section, providers);
            }
        }

        private IConfigurationProvider FindLastProvider(string path, IList<IConfigurationProvider> providers)
        {
            for (int i = providers.Count - 1; i >= 0; i--)
            {
                IConfigurationProvider prov = providers[i];
                if (prov.TryGet(path, out string val))
                    return prov;
            }

            return null;
        }

        public void Dump(IConfigurationRoot config)
        {
            dumpAction("ConfigDump start");
            DumpConfig(config, new List<IConfigurationProvider>(config.Providers));
            dumpAction("ConfigDump end");
        }
    }
}