﻿namespace Bastion.Kernel
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// Class that implements file system routines
    /// </summary>
    public static class FS
    {
        /// <summary>
        /// Copies one folder into another.
        /// </summary>
        /// <param name="sourceDirName">Source folder to copy from.</param>
        /// <param name="destDirName">Destination folder to copy into.</param>
        /// <param name="copySubDirs">Indicates whether subfolders should be copied as well.</param>
        public static void CopyDirectory(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // do not copy directory to itself
            if (string.Equals(sourceDirName, destDirName, StringComparison.OrdinalIgnoreCase))
            {
                throw new Exception("Copying directory over itself is prohibited");
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                if (!File.Exists(temppath))// does not work, why?
                {
                    file.CopyTo(temppath, true);
                }
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    CopyDirectory(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        /// <summary>
        /// Outputs content of the folder to console.
        /// </summary>
        /// <param name="path">Folder path to outpout content of.</param>
        /// <param name="relative">Indicates whether to ouput relative path names.</param>
        public static void Ls(string path, bool relative = true)
        {
            foreach (string file in GetFiles(path))
            {
                Console.WriteLine(
                    relative
                        ? file.Substring(path.Length + 1)
                        : file);
            }
        }

        /// <summary>
        /// Returns all filenames in the specified location.
        /// </summary>
        /// <param name="path">Path to the location with files.</param>
        /// <param name="recursive">Indicates whether to recursively return all files names in all subfolders.</param>
        /// <returns>Collection of filenames.</returns>
        public static IEnumerable<string> GetFiles(string path, bool recursive = true)
        {
            SearchOption options = recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            foreach (string file in Directory.EnumerateFiles(path, "*.*", options))
            {
                yield return file;
            }
        }
    }
}